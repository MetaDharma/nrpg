export default class SCompendium extends Compendium {
  /** @inheritdoc */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["s-compendium"],
      template: "systems/n5e/templates/journal/s-compendium.hbs",
      width: 800,
      height: 855,
      resizable: true
    });
  }

  /* -------------------------------------------- */

  /**
   * The IDs of some special pages that we use when configuring the display of the compendium.
   * @type {Object<string>}
   * @protected
   */
  static _SPECIAL_PAGES = {
    chakraItemList: "sfJtvPjEs50Ruzi4",
    jutsuList: "plCB5ei1JbVtBseb"
  };

  /* -------------------------------------------- */

  /** @inheritdoc */
  async getData(options) {
    const data = await super.getData(options);
    const documents = await this.collection.getDocuments();
    const getOrder = o => ({chapter: 0, appendix: 100}[o.flags?.n5e?.type] ?? 200) + (o.flags?.n5e?.position ?? 0);
    data.chapters = documents.reduce((arr, entry) => {
      const type = entry.getFlag("n5e", "type");
      if ( !type ) return arr;
      const e = entry.toObject();
      e.showPages = (e.pages.length > 1) && (type === "chapter");
      arr.push(e);
      return arr;
    }, []).sort((a, b) => getOrder(a) - getOrder(b));
    // Add jutsu A-Z to the end of Chapter 10.
    const jutsuList = this.collection.get(this.constructor._SPECIAL_PAGES.jutsuList);
    data.chapters[9].pages.push({_id: jutsuList.id, name: jutsuList.name, entry: true});
    // Add chakra items A-Z to the end of Chapter 11.
    const chakraItemList = this.collection.get(this.constructor._SPECIAL_PAGES.chakraItemList);
    data.chapters[10].pages.push({_id: chakraItemList.id, name: chakraItemList.name, entry: true});
    return data;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  activateListeners(html) {
    super.activateListeners(html);
    html.find("a").on("click", this._onClickLink.bind(this));
  }

  /* -------------------------------------------- */

  /**
   * Handle clicking a link to a journal entry or page.
   * @param {MouseEvent} event  The triggering click event.
   * @protected
   */
  async _onClickLink(event) {
    const target = event.currentTarget;
    const entryId = target.closest("[data-entry-id]")?.dataset.entryId;
    const pageId = target.closest("[data-page-id]")?.dataset.pageId;
    if ( !entryId ) return;
    const options = {};
    if ( pageId ) options.pageId = pageId;
    const entry = await this.collection.getDocument(entryId);
    entry?.sheet.render(true, options);
  }
}