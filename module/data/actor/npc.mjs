import { FormulaField } from "../fields.mjs";
import AttributesFields from "./templates/attributes.mjs";
import CreatureTemplate from "./templates/creature.mjs";
import DetailsFields from "./templates/details.mjs";
import TraitsFields from "./templates/traits.mjs";

/**
 * System data definition for NPCs.
 *
 * @property {object} attributes
 * @property {object} attributes.ac
 * @property {number} attributes.ac.flat         Flat value used for flat or natural armor calculation.
 * @property {string} attributes.ac.calc         Name of one of the built-in formulas to use.
 * @property {string} attributes.ac.formula      Custom formula to use.
 * @property {object} attributes.cp
 * @property {number} attributes.cp.value        Current chakra points.
 * @property {number} attributes.cp.max          Maximum allowed CP value.
 * @property {number} attributes.cp.temp         Temporary CP applied on top of value.
 * @property {number} attributes.cp.tempmax      Temporary change to the maximum CP.
 * @property {string} attributes.cp.formula      Formula used to determine chakra points.
 * @property {object} attributes.hp
 * @property {number} attributes.hp.value        Current hit points.
 * @property {number} attributes.hp.max          Maximum allowed HP value.
 * @property {number} attributes.hp.temp         Temporary HP applied on top of value.
 * @property {number} attributes.hp.tempmax      Temporary change to the maximum HP.
 * @property {string} attributes.hp.formula      Formula used to determine hit points.
 * @property {object} attributes.td
 * @property {number} attributes.td.value        Current tenacity die points.
 * @property {number} attributes.td.denom        Current tenacity die points.
 * @property {number} attributes.td.max          Maximum allowed tenacity die value.
 * @property {object} details
 * @property {TypeData} details.type             Creature type of this NPC.
 * @property {string} details.type.value         NPC's type as defined in the system configuration.
 * @property {string} details.type.subtype       NPC's subtype usually displayed in parenthesis after main type.
 * @property {string} details.type.rank          Rank of the NPC.
 * @property {string} details.type.swarm         Size of the individual creatures in a swarm, if a swarm.
 * @property {string} details.type.custom        Custom type beyond what is available in the configuration.
 * @property {string} details.environment        Common environments in which this NPC is found.
 * @property {number} details.npcLevel           NPC's level.
 * @property {string} details.source             What book or adventure is this NPC from?
 * @property {number} details.xp
 * @property {number} details.xp.value           Amount of experience points gained from defeating this NPC.
 * @property {object} resources
 * @property {object} resources.litact           NPC's elite actions.
 * @property {number} resources.litact.value     Currently available elite actions.
 * @property {number} resources.litact.max       Maximum number of elite actions.
 * @property {object} resources.litres           NPC's elite resistances.
 * @property {number} resources.litres.value     Currently available elite resistances.
 * @property {number} resources.litres.max       Maximum number of elite resistances.
 */
export default class NPCData extends CreatureTemplate {

  /** @inheritdoc */
  static _systemType = "npc";

  /* -------------------------------------------- */

  /** @inheritdoc */
  static defineSchema() {
    return this.mergeSchema(super.defineSchema(), {
      attributes: new foundry.data.fields.SchemaField({
        ...AttributesFields.common,
        ...AttributesFields.creature,
        ac: new foundry.data.fields.SchemaField({
          flat: new foundry.data.fields.NumberField({integer: true, min: 0, label: "N5E.ArmorClassFlat"}),
          calc: new foundry.data.fields.StringField({initial: "default", label: "N5E.ArmorClassCalculation"}),
          formula: new FormulaField({deterministic: true, label: "N5E.ArmorClassFormula"})
        }, {label: "N5E.ArmorClass"}),
        cp: new foundry.data.fields.SchemaField({
            value: new foundry.data.fields.NumberField({
              nullable: false, integer: true, min: 0, initial: 10, label: "N5E.ChakraPointsCurrent"
            }),
            max: new foundry.data.fields.NumberField({
              nullable: false, integer: true, min: 0, initial: 10, label: "N5E.ChakraPointsMax"
            }),
            temp: new foundry.data.fields.NumberField({integer: true, initial: 0, min: 0, label: "N5E.ChakraPointsTemp"}),
            tempmax: new foundry.data.fields.NumberField({integer: true, initial: 0, label: "N5E.ChakraPointsTempMax"}),
            formula: new FormulaField({required: true, label: "N5E.CPFormula"})
          }, {label: "N5E.ChakraPoints"}),
        hp: new foundry.data.fields.SchemaField({
          value: new foundry.data.fields.NumberField({
            nullable: false, integer: true, min: 0, initial: 10, label: "N5E.HitPointsCurrent"
          }),
          max: new foundry.data.fields.NumberField({
            nullable: false, integer: true, min: 0, initial: 10, label: "N5E.HitPointsMax"
          }),
          temp: new foundry.data.fields.NumberField({integer: true, initial: 0, min: 0, label: "N5E.HitPointsTemp"}),
          tempmax: new foundry.data.fields.NumberField({integer: true, initial: 0, label: "N5E.HitPointsTempMax"}),
          formula: new FormulaField({required: true, label: "N5E.HPFormula"})
        }, {label: "N5E.HitPoints"}),
        td: new foundry.data.fields.SchemaField({
            value: new foundry.data.fields.NumberField({
              nullable: false, integer: false, min: 0, initial: 10, label: "N5E.TenacityDiceCurrent"
            }),
            max: new foundry.data.fields.NumberField({
              nullable: false, integer: true, min: 0, initial: 10, label: "N5E.TenacityDiceMax"
            }),
            denom: new FormulaField({required: false, label: "N5E.HPFormula"})
          }, {label: "N5E.TenacityDice"})
      }, {label: "N5E.Attributes"}),
      details: new foundry.data.fields.SchemaField({
        ...DetailsFields.common,
        ...DetailsFields.creature,
        type: new foundry.data.fields.SchemaField({
          value: new foundry.data.fields.StringField({required: true, blank: true, label: "N5E.CreatureType"}),
          subtype: new foundry.data.fields.StringField({required: true, label: "N5E.CreatureTypeSelectorSubtype"}),
          rank: new foundry.data.fields.StringField({required: true, blank: true, label: "N5E.CreatureRank"}),
          swarm: new foundry.data.fields.StringField({required: true, blank: true, label: "N5E.CreatureSwarmSize"}),
          custom: new foundry.data.fields.StringField({required: true, label: "N5E.CreatureTypeSelectorCustom"})
        }, {label: "N5E.CreatureType"}),
        environment: new foundry.data.fields.StringField({required: true, label: "N5E.Environment"}),
        npcLevel: new foundry.data.fields.NumberField({
          required: true, nullable: false, min: 0, initial: 1, label: "N5E.Level"
        }),
        source: new foundry.data.fields.StringField({required: true, label: "N5E.Source"}),
        xp: new foundry.data.fields.SchemaField({
            value: new foundry.data.fields.StringField({required: true, label: "N5E.ExperiencePointsCurrent"})
        }, {label: ""}),
      }, {label: "N5E.ExperiencePointsAbbr"}),
      resources: new foundry.data.fields.SchemaField({
        litact: new foundry.data.fields.SchemaField({
          value: new foundry.data.fields.NumberField({
            required: true, nullable: false, integer: true, min: 0, initial: 0, label: "N5E.LitActRemaining"
          }),
          max: new foundry.data.fields.NumberField({
            required: true, nullable: false, integer: true, min: 0, initial: 0, label: "N5E.LitActMax"
          })
        }, {label: "N5E.LitAct"}),
        litres: new foundry.data.fields.SchemaField({
          value: new foundry.data.fields.NumberField({
            required: true, nullable: false, integer: true, min: 0, initial: 0, label: "N5E.LitResRemaining"
          }),
          max: new foundry.data.fields.NumberField({
            required: true, nullable: false, integer: true, min: 0, initial: 0, label: "N5E.LitResMax"
          })
        }, {label: "N5E.LitRes"})
      }, {label: "N5E.Resources"}),
      traits: new foundry.data.fields.SchemaField({
        ...TraitsFields.common,
        ...TraitsFields.creature
      }, {label: "N5E.Traits"})
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  static migrateData(source) {
    super.migrateData(source);
    NPCData.#migrateTypeData(source);
    AttributesFields._migrateInitiative(source.attributes);
  }

  /* -------------------------------------------- */

  /**
   * Migrate the actor type string to type object.
   * @param {object} source  The candidate source data from which the model will be constructed.
   */
  static #migrateTypeData(source) {
    const original = source.type;
    if ( typeof original !== "string" ) return;

    source.type = {
      value: "",
      subtype: "",
      swarm: "",
      custom: ""
    };

    // Match the existing string
    const pattern = /^(?:swarm of (?<size>[\w-]+) )?(?<type>[^(]+?)(?:\((?<subtype>[^)]+)\))?$/i;
    const match = original.trim().match(pattern);
    if ( match ) {

      // Match a known creature type
      const typeLc = match.groups.type.trim().toLowerCase();
      const typeMatch = Object.entries(CONFIG.N5E.creatureTypes).find(([k, v]) => {
        return (typeLc === k)
          || (typeLc === game.i18n.localize(v).toLowerCase())
          || (typeLc === game.i18n.localize(`${v}Pl`).toLowerCase());
      });
      if ( typeMatch ) source.type.value = typeMatch[0];
      else {
        source.type.value = "custom";
        source.type.custom = match.groups.type.trim().titleCase();
      }
      source.type.subtype = match.groups.subtype?.trim().titleCase() ?? "";

      // Match a swarm
      if ( match.groups.size ) {
        const sizeLc = match.groups.size ? match.groups.size.trim().toLowerCase() : "tiny";
        const sizeMatch = Object.entries(config.N5E.actorSizes).find(([k, v]) => {
          return (sizeLc === k) || (sizeLc === game.i18n.localize(v).toLowerCase());
        });
        source.type.swarm = sizeMatch ? sizeMatch[0] : "tiny";
      }
      else source.type.swarm = "";
    }

    // No match found
    else {
      source.type.value = "custom";
      source.type.custom = original;
    }
  }
}