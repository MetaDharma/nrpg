import { SparseDataModel } from "../abstract.mjs";
import { MappingField } from "../fields.mjs";

/**
 * Data model for the Ability Score Improvement advancement configuration.
 *
 * @property {number} points                 Number of points that can be assigned to any score.
 * @property {Object<string, number>} fixed  Number of points automatically assigned to a certain score.
 */
export class AbilityScoreImprovementConfigurationData extends foundry.abstract.DataModel {
  /** @inheritdoc */
  static defineSchema() {
    return {
      // TODO: This should default to 1 if added to a class, or 0 if added to anything else
      points: new foundry.data.fields.NumberField({
        integer: true, min: 0, initial: 1,
        label: "N5E.AdvancementAbilityScoreImprovementPoints",
        hint: "N5E.AdvancementAbilityScoreImprovementPointsHint"
      }),
      fixed: new MappingField(
        new foundry.data.fields.NumberField({nullable: false, integer: true, initial: 0}),
        {label: "N5E.AdvancementAbilityScoreImprovementFixed"}
      )
    };
  }
}

/**
 * Data model for the Ability Score Improvement advancement value.
 *
 * @property {Object<string, number>}  Points assigned to individual scores.
 */
export class AbilityScoreImprovementValueData extends SparseDataModel {
  /** @inheritdoc */
  static defineSchema() {
    return {
      assignments: new MappingField(new foundry.data.fields.NumberField({
        nullable: false, integer: true
      }), {required: false, initial: undefined}),
    };
  }
}